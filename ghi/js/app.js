function createCard(name, venue, description, pictureUrl, starts, ends) {
    return `
        <div class="card .col-sm-6 m-3 mb-3 bg-dark rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${venue}</h6>
                <p class="card-text">${description}</p>
                <p class="card-footer">${starts} - ${ends}</p>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const error = await response.json();
            const errorMessage = error.message || 'An error occurred while fetching the data.';
            const alert = `
                <div class="alert alert-danger" role="alert">
                    ${errorMessage}
                </div>
            `;
            const container = document.querySelector('.container');
            container.innerHTML += alert;
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const starts = new Date(details.conference.starts).toDateString();
                    const ends = new Date(details.conference.ends).toDateString();
                    const venue = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, venue, description, pictureUrl, starts, ends);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                } else {
                    const error = await detailResponse.json();
                    const errorMessage = error.message || 'An error occurred while fetching the conference details.';
                    const alert = `
                        <div class="alert alert-danger" role="alert">
                            ${errorMessage}
                        </div>
                    `;
                    const container = document.querySelector('.container');
                    container.innerHTML += alert;
                }
            }
        }
    } catch (e) {
        console.error(e);
        const errorMessage = 'An error occurred while processing the data.';
        const alert = `
            <div class="alert alert-danger" role="alert">
                ${errorMessage}
            </div>
        `;
        const container = document.querySelector('.container');
        container.innerHTML += alert;
    }
});
